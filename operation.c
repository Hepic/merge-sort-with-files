#include <stdio.h>
#include <stdlib.h>
#include "operation.h"

char fname[100];

void merge(int ind) {
    char fname1[100], fname2[100];
    sprintf(fname1, "temp/temp%d.dat", 2 * ind + 1);
    sprintf(fname2, "temp/temp%d.dat", 2 * ind + 2);

    FILE *tmp1 = fopen(fname1, "rb");
    FILE *tmp2 = fopen(fname2, "rb");

    fseek(tmp1, 0, SEEK_END);
    fseek(tmp2, 0, SEEK_END);

    int len1 = ftell(tmp1) / sizeof(double);
    int len2 = ftell(tmp2) / sizeof(double);
    
    rewind(tmp1);
    rewind(tmp2);

    sprintf(fname, "temp/temp%d.dat", ind);
    FILE *tmp = fopen(fname, "wb");

    int p1 = 0, p2 = 0;
    
    // implement the merge technique
    while (p1 < len1 || p2 < len2) {
        double val1 = -1, val2 = -1;
        
        // read each time the numbers from the children of current node in the mergeSort tree
        if (p1 < len1) {
            fread((void*)(&val1), sizeof(double), 1, tmp1);
        }

        if (p2 < len2) {
            fread((void*)(&val2), sizeof(double), 1, tmp2);
        }
        
        // write each time in the node of the mergeSort tree the minimum number
        if (p2 == len2 || (p1 < len1 && val1 <= val2)) {
            fwrite((void*)(&val1), sizeof(double), 1, tmp);
            fseek(tmp2, -sizeof(double), SEEK_CUR);
            ++p1;
        } else {
            fwrite((void*)(&val2), sizeof(double), 1, tmp);
            fseek(tmp1, -sizeof(double), SEEK_CUR);
            ++p2;
        }
    }

    fclose(tmp);
    fclose(tmp1);
    fclose(tmp2);

    remove(fname1);
    remove(fname2);
}

void mergeSort(FILE *inp, int Tbeg, int Tend, int ind) {
    if (Tbeg == Tend) { // this is the base case
        sprintf(fname, "temp/temp%d.dat", ind);
        FILE *tmp = fopen(fname, "wb");
        double val;
        
        // read the number from the position Tbeg
        fseek(inp, Tbeg * sizeof(double), SEEK_SET);
        fread((void*)(&val), sizeof(double), 1, inp);
        
        // write the number to a temporary file
        fwrite((void*)(&val), sizeof(double), 1, tmp);

        fclose(tmp);
        return;
    }
    
    int Tmid = (Tbeg + Tend) >> 1;
    
    // call the mergeSort recursively
    mergeSort(inp, Tbeg, Tmid, 2 * ind + 1);
    mergeSort(inp, Tmid + 1, Tend, 2 * ind + 2);
    merge(ind);
}

int checkSorted(FILE *out) {
    fseek(out, 0, SEEK_END);

    int len = ftell(out) / sizeof(double);
    double val1, val2;
    
    rewind(out);
    fread((void*)(&val1), sizeof(double), 1, out);
    
    // compare each number with its next one.
    for (int i = 1; i < len; ++i) {
        fread((void*)(&val2), sizeof(double), 1, out);

        if (val1 > val2) {
            return 0;
        }

        val1 = val2;
    }

    return 1;
}

void printFile(FILE *fp) {
    fseek(fp, 0, SEEK_END);

    int len = ftell(fp) / sizeof(double);
    double val;

    rewind(fp);

    for (int i = 0; i < len; ++i) {
        fread((void*)(&val), sizeof(double), 1, fp);
        printf("%lf ", val);
    }
    
    printf("\n");
}
