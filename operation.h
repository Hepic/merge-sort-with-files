#ifndef OPERATION_H
#define OPERATION_H

void mergeSort(FILE *, int, int, int);
int checkSorted(FILE *);
void printFile(FILE *);

#endif
