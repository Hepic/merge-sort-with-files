#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "operation.h"

int main(int argc, char *argv[]) {
    srand(time(0));
    
    if (argc != 3) {
        fprintf(stderr, "Error with the number of arguments\n");
        return 0;
    }
   
    struct stat st = {0};

    if (stat("temp", &st) == -1) {
        mkdir("temp", 0700);
    }
    
    int N = atoi(argv[1]);
    char *outFile = argv[2];
    FILE *inp = fopen("temp/input.dat", "wb");
    double val;

    for (int i = 0; i < N; ++i) {
        double num = 200 * (double)rand() / RAND_MAX - 100; // generate random float numbers
        fwrite((void*)(&num), sizeof(double), 1, inp); // write the numbers in the input.dat file
    }
    
    fclose(inp);
    
    inp = fopen("temp/input.dat", "rb");
    mergeSort(inp, 0, N - 1, 0); // sort the numbers calling the merge-sort algorithm
    fclose(inp);
    
    FILE *tmp = fopen("temp/temp0.dat", "rb");
    FILE *out = fopen(outFile, "wb");
    
    // write in the output file the sorted numbers
    for (int i = 0; i < N; ++i) {
        fread((void*)(&val), sizeof(double), 1, tmp);
        fwrite((void*)(&val), sizeof(double), 1, out);
    }
    
    fclose(tmp);
    fclose(out);
    
    remove("temp/input.dat");
    remove("temp/temp0.dat");
    
    out = fopen(outFile, "rb");
    printf("File %s is%s sorted\n", outFile, checkSorted(out) ? "" : " not");
    
    printFile(out); // debug mode

    fclose(out);
    return 0;
}
