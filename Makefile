CC=gcc
CFLAGS=-I.
DEPS = operation.h

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

run: oocmerge.o operation.o
	$(CC) oocmerge.o operation.o -o run
